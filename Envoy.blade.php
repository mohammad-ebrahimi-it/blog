@servers(['web' => 'deployer@serverip'])@setup
$repository = 'git@gitlab.com:mohammad-ebrahimi-it/blog.git';
$releases_dir = '/home/oem/site/www/blog';
$app_dir = '/home/oem/site/www';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
$password = 'm123456789';//deployer user password
@endsetup@story('deploy')
clone_repository
run_composer
update_symlinks
@endstory@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}
@endtask@task('run_composer')
echo "Starting deployment ({{ $release }})"
cd {{ $new_release_dir }}
composer install --prefer-dist --no-scripts -q -o
@endtask@task('update_symlinks')
echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
echo "{{ $password }}" | sudo -S chmod 777 -R {{ $app_dir }}
@endtask
